//Enforces that the passwords match
function checkForm() {
    var pass1 = document.getElementById("P1").value;
    var pass2 = document.getElementById("P2").value;
    if (pass1 != pass2) {
        alert("Passwords do not match!");
        document.getElementById("P1").style.borderColor = "#E34234";
        document.getElementById("P2").style.borderColor = "#E34234";
        return false;
    } else {
        return true;
    }
}

function checkPassword() {
    //Adding username to the word blacklist
    var username = document.getElementById("U").value;
    var words = [];
    words[0] = username;
    //Getting results
    var password = document.getElementById("P1").value;
    var res = zxcvbn(password,words);
    //Colors the correspond to the scores of passwords
    var colorCodes = {
        0: "#800000",
        1: "#FFA500",
        2: "#9ACD32",
        3: "#008000",
        4: "#006400"
    }
    //Gathering document elements for later modification and styling
    var scoreElem = document.getElementById("score");
    var pwBorderElem = document.getElementById("passwordborder");
    var warningsElem = document.getElementById("warnings");
    var suggestionsElem = document.getElementById("suggestions");
    //Filled border always comes up whenever selected, or a character is entered
    pwBorderElem.setAttribute("style","border-radius: 15px; background: #73AD21; padding: 5px;");
    //Score
    if (password) {
        scoreElem.style.visibility = "visible";
        scoreElem.innerHTML = "Score: <center><img src=\"Scores/" +
            (res.score+1) + ".png\"/>" + "</center><br />Crack time: " +
             res.crack_times_display["offline_slow_hashing_1e4_per_second"] +  "<br />";
        //Images from http://www.oksanaorda.com/vendor/emoji/data/sheet_google_64.png
        //Password Border
        scoreElem.style.background = colorCodes[res.score];
    } else {
        scoreElem.innerHTML = "";
        scoreElem.style.visibility = "hidden";
    }
    //Warnings
    if (res.feedback.warning && password) { // Only show warning box if theres a warning and pw
        warningsElem.style.visibility = "visible";
        warningsElem.style.height = 75;
        warningsElem.innerHTML = "Warnings: <br /> " + res.feedback.warning+ "<br />";
    } else {
        warningsElem.style.visibility = "hidden";
        warningsElem.style.height = 0;
    }

    //Suggestions
    if ( res.feedback.suggestions.length) { //Show suggestions if applicable. There can be suggestions at 0 length pw
        suggestionsElem.style.visibility = "visible";
        suggestionsElem.style.height = 75;
        suggestionsElem.innerHTML = "Suggestions: <br /> " + res.feedback.suggestions + "<br />";
    } else {
        suggestionsElem.style.visibility = "hidden";
        suggestionsElem.style.height = 0;
    }
}

function clearDecorations() {
    var scoreElem = document.getElementById("score");
    var pwBorderElem = document.getElementById("passwordborder");
    var warningsElem = document.getElementById("warnings");
    var suggestionsElem = document.getElementById("suggestions");
    pwBorderElem.setAttribute("style","");
    scoreElem.innerHTML = "";
    scoreElem.style.visibility = "hidden";
    warningsElem.style.visibility = "hidden";
    warningsElem.style.height = 0;
    suggestionsElem.style.visibility = "hidden";
    suggestionsElem.style.height = 0;
}
