//Enforces that the passwords match
function checkForm() {
    var pass1 = document.getElementById("P1").value;
    var pass2 = document.getElementById("P2").value;
    if (pass1 != pass2) {
        alert("Passwords do not match!");
        document.getElementById("P1").style.borderColor = "#E34234";
        document.getElementById("P2").style.borderColor = "#E34234";
        return false;
    } else {
        return true;
    }
}
